#include "Ultrasonic.h"

#define US_TRG_PIN 30
#define US_ECH_PIN 31
#define LDR_PIN A0

Ultrasonic ultrasonic(US_TRG_PIN, US_ECH_PIN);

struct sensor {
  byte id;
  char *nome;
  float val;
  float (*le)(struct sensor);
};
  
float leSensorDefault(struct sensor s){
  s.val = s.le(s);
}

struct sensor sensores[] = {
  {id: 0, nome: "ldr", le: &leSensorDefault },
  {id: 1, nome: "ultrasom", le: &leSensorDefault },
};

void setup() {
  Serial.begin(9600);
}

void loop()
{
  // le sensores
  //ldr = analogRead(LDR_PIN);
  //usCM = ultrasonic.Ranging(CM);
  
  //sendSensorValue(1, ldr);
  //sendSensorValue(2, usCM);
  
  delay(500);
}

float leUltrasom(){}
/*
void sendSensorValue(int id, float value){
  Serial.print("{\"id\":");
  Serial.print(id);
  Serial.print(",\"val\":");
  Serial.print(value);
  Serial.print("}");
  Serial.println();
}

void sendSensorsValues(){
  
}
*/


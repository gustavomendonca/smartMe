#Apresentação

Boa tarde à todos, nós somos a equipe smartMe e estamos aqui hoje para apresentar à vocês a ideia de nosso produto.
[Apresentação da equipe]

#Introdução
Definir Internet das coisas é uma árdua tarefa. O próprio termo não poderia ser mais genérico: coisas. É mais fácil entender o que a internet das coisas pode fazer por você do que defini-la. Existem diferentes níveis que se pode utilizar a IoT.  Prover conectividade à um objeto simplesmente encurta sua distância. Automatizar a coleta de dados ou acionamentos é simplesmente isso: uma automação. Mas o verdadeiro valor na IoT estão nas POSSIBILIDADES. Ao se ter uma representação virtual de um objeto na rede, é possível utilizar de todos os recursos disponíveis nesta para interagir e promover inteligência aos objetos

#Slide Paula
Paula é uma mulher ativa, com uma rotina intensa. Ela vive em uma cidade grande onde a possibilidade de atrasar é sempre presente. 
(falar o que thiago disse..)

#Slide outro-cara
(falar o que mariana disse)

(esses dois acabam definindo o publico alvo)

#Slide espelho conceito
Paula e outro-cara valorizam seu tempo, e precisam de ajuda! Nossa solução: SmartMe. Um espelho de banheiro (ou não!) que exibe e reliza sugestões para o usuário baseado em sua rotina. Espelho identifica a presença do usuário e realiza sugestões:
(frases a tela):
-6h23 e você ainda aqui? -> não vai se atrasar? trânsito intenso na cidade!
-Previsão de chuva: -> leve o guarda-chuva!
-Compromisso no google calendar-> Reunião às 14h00 com José
Mensagem deixada pelo companheiro/a: 'boa sorte na entrevista de hoje!'
O produto é personalizável pelo próprio usuário e pode conter inúmeros 'widgets': relógio, calendário, compromissos, trânsito, bolsa de valores, notícias.
Interação via comandos de voz simples, para evitar sujar o espelho. 
Integração com eletrodomésticos como balança do banheiro e cafeteira.
Apenas uma parte do espelho é de área projetável, suas margens são compostas por iluminação com intensidade ajustada pelo espelho.

#A Tecnologia
Toda tecnologia necessária já está disponível. O produto baseia-se em um monitor lcd com um vidro com película espelhada na frente. Um raspberry pi seria suficiente para rodar o sistema.

#Alexia, Cortana, Siri, Google Home... Concorrentes?
Não, produtos complementares, espelho poderia interagir com estes novos produtos. "Mensagem de Alexia: seu capputino está pronto!"

#Preço
(Falar o que mariana falou sobre o fato de espelhos já serem caros. Que poderia ser utililizado uma pequena area de projecao em um espelho grande.

#Barreiras de Produção
Produzir hardware no Brasil possui implicações, porém o produto não exige hardware especial. Composto por um monitor LCD e um raspberry pi com poucos sensores.

# Por onde comecar?
Estabelecimentos comerciais como Bares, Restaurantes e Elevadores. Pequena produção, sob encomenda e pode ser manufaturado pela equipe. A flexibilidade do produto faz com se seja fácil desenvolver funcionalidades que agregem valor ao cliente (selfie em redes sociais, propagandas iterativas...)


(function () {
  'use strict'

  const express = require('express')
  const app = express()
  const bodyParser = require('body-parser')
  const path = require('path')
  const port = process.env.SMART_ME_API_PORT || 3000

  app.use(bodyParser.json())
  app.use(express.static('./frontend'))
  
  app.use((req, res, next) => {
    let now = new Date
    console.log(`[${now.toISOString().replace('T', ' ').slice(0, 23)}] ${req.method} em ${req.path}`)
    next()
  })
  
  // inicia servidor
  app.listen(port, () =>  {
    console.log('server iniciado na porta', port)
  })

})()
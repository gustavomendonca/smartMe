(function () {
  'use strict'

  angular
    .module('smartme')
    .directive('clock', ($interval) => {
      return {
        scope: true,
        transclude: true,
        template: "<span class='clock'><span class='clock-text' ng-transclude></span><span class='clock-time'>{{date.now() | date: timeFormat}}</span></span>",
        link: function ($scope, $elem, $attr) {
            $scope.timeFormat = ($attr.format === '12') ? 'hh:mm:ss a' : 'HH:mm:ss'
            $scope.date = Date
            $interval(() => {}, 1000)
        }
      }
    })

})()
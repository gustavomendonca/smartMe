(function () {
  'use strict'

  angular
    .module('smartme')
    .config(routeConfig)

  function routeConfig($routeProvider, $locationProvider) {
    $routeProvider
      .when("/", {
        templateUrl: "pages/desktop.html",
        controller: "DesktopController",
      })
      .otherwise({
        templateUrl: "erro.html"
      })

    $locationProvider.html5Mode(false)
    $locationProvider.hashPrefix('!')

  }

})()